type Point2D = {
  x: number;
  y: number;
};

function createPoint2D(x: number, y: number) {
  return {
    x,
    y,
  };
}

function isPointInCircle(p: Point2D, radius: number) {
  return Math.sqrt(p.x * p.x + p.y * p.y) <= radius;
}

function arePointsInCircle(points: Point2D[], radius: number) {
  return points.every(p => isPointInCircle(p, radius));
}

export { createPoint2D, isPointInCircle, arePointsInCircle, Point2D };
