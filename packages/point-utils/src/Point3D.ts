type Point3D = {
  x: number;
  y: number;
  z: number;
};

function createPoint3D(x: number, y: number, z: number) {
  return {
    x,
    y,
    z,
  };
}

function isPointInSphere(p: Point3D, radius: number) {
  return Math.sqrt(p.x * p.x + p.y * p.y + p.z * p.z) <= radius;
}

function arePointsInSphere(points: Point3D[], radius: number) {
  return points.every(p => isPointInSphere(p, radius));
}

export { createPoint3D, isPointInSphere, arePointsInSphere, Point3D };
