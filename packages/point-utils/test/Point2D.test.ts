import { createPoint2D, isPointInCircle, arePointsInCircle } from '../src';

describe('Point2D', () => {
  it('Can check if a points lies in a circle', () => {
    const p1 = createPoint2D(1, 2);
    const p2 = createPoint2D(1, -2);
    const p3 = createPoint2D(-1, -2);

    expect(isPointInCircle(p1, 2.2)).toBe(false);
    expect(isPointInCircle(p1, 2.3)).toBe(true);
    expect(isPointInCircle(p2, 2.2)).toBe(false);
    expect(isPointInCircle(p2, 2.3)).toBe(true);
    expect(isPointInCircle(p3, 2.2)).toBe(false);
    expect(isPointInCircle(p3, 2.3)).toBe(true);
  });

  it('Can check if multiple points lie in a circle', () => {
    const p1 = createPoint2D(1, 2);
    const p2 = createPoint2D(1, -2);
    const p3 = createPoint2D(-1, -2);
    const p4 = createPoint2D(2, 2);

    expect(arePointsInCircle([p1, p2, p3], 2.2)).toBe(false);
    expect(arePointsInCircle([p1, p2, p3], 2.3)).toBe(true);
    expect(arePointsInCircle([p1, p2, p3, p4], 2.3)).toBe(false);
  })
});
