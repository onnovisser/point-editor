import { createPoint3D, isPointInSphere, arePointsInSphere } from '../src';

describe('Point3D', () => {
  it('Can check if a points lies in a sphere', () => {
    const p1 = createPoint3D(1, 2, 3);
    const p2 = createPoint3D(1, -2, 3);
    const p3 = createPoint3D(-1, -2, 3);

    expect(isPointInSphere(p1, 3.7)).toBe(false);
    expect(isPointInSphere(p1, 3.8)).toBe(true);
    expect(isPointInSphere(p2, 3.7)).toBe(false);
    expect(isPointInSphere(p2, 3.8)).toBe(true);
    expect(isPointInSphere(p3, 3.7)).toBe(false);
    expect(isPointInSphere(p3, 3.8)).toBe(true);
  });

  it('Can check if multiple points lie in a sphere', () => {
    const p1 = createPoint3D(1, 2, 3);
    const p2 = createPoint3D(1, -2, 3);
    const p3 = createPoint3D(-1, -2, 3);
    const p4 = createPoint3D(2, 2, 3);

    expect(arePointsInSphere([p1, p2, p3], 3.7)).toBe(false);
    expect(arePointsInSphere([p1, p2, p3], 3.8)).toBe(true);
    expect(arePointsInSphere([p1, p2, p3, p4], 3.8)).toBe(false);
  })
});
