import { func, number, shape, string } from 'prop-types';
import React, { useState } from 'react';
import GroupForm from './GroupForm';
import Point from './Point';
import PointForm from './PointForm';

Group.propTypes = {
  group: shape({
    id: string.isRequired,
    radius: number.isRequired,
  }).isRequired,
  onRemove: func.isRequired,
  onChange: func.isRequired,
  onAddPoint: func.isRequired,
  onRemovePoint: func.isRequired,
  onChangePoint: func.isRequired,
};

function Group({
  group,
  onRemove,
  onChange,
  onAddPoint,
  onRemovePoint,
  onChangePoint,
}) {
  const [isEditing, setIsEditing] = useState(false);

  function handleEditSubmit(newPoint) {
    setIsEditing(false);
    onChange(newPoint);
  }

  return (
    <details>
      <summary>
        {isEditing ? (
          <>
            <GroupForm
              group={group}
              onSubmit={handleEditSubmit}
              initialValues={{ radius: group.radius }}
              submitLabel="confirm"
            />
            <button onClick={() => setIsEditing(false)}>cancel</button>
          </>
        ) : (
          <>
            Group (radius: {group.radius})
            <button onClick={() => setIsEditing(true)}>edit group</button>
            <button onClick={onRemove}>remove</button>
          </>
        )}
      </summary>
      <PointForm group={group} onSubmit={onAddPoint} submitLabel="Add point" />
      {group.points.map(p => (
        <Point
          group={group}
          onRemove={() => onRemovePoint(p.id)}
          onChange={point => onChangePoint(p.id, point)}
          key={p.id}
          point={p.point}
        />
      ))}
    </details>
  );
}

export default Group;
