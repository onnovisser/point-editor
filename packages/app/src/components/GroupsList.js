import React, { useContext } from 'react';
import Group from './Group';
import GroupForm from './GroupForm';
import { pointsStoreContext } from './PointsStore';

function GroupsList() {
  const [state, dispatch] = useContext(pointsStoreContext);

  return (
    <>
      <GroupForm
        onSubmit={({ radius }) => {
          dispatch({ type: 'addGroup', payload: { radius } });
        }}
        submitLabel="Add group"
      />
      <ul>
        {state.groups.map(group => (
          <li key={group.id}>
            <Group
              group={group}
              onChange={({ radius }) => {
                dispatch({
                  type: 'editGroup',
                  payload: { id: group.id, radius },
                });
              }}
              onRemove={() =>
                dispatch({
                  type: 'removeGroup',
                  payload: { id: group.id },
                })
              }
              onAddPoint={point =>
                dispatch({
                  type: 'addPointToGroup',
                  payload: { groupId: group.id, point },
                })
              }
              onRemovePoint={id =>
                dispatch({
                  type: 'removePointFromGroup',
                  payload: { groupId: group.id, pointId: id },
                })
              }
              onChangePoint={(id, point) =>
                dispatch({
                  type: 'editPoint',
                  payload: { groupId: group.id, pointId: id, point },
                })
              }
            />
          </li>
        ))}
      </ul>
    </>
  );
}

export default GroupsList;
