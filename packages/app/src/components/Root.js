import React from 'react';
import GroupsList from './GroupsList';
import PointsStore from './PointsStore';

function Root() {
  return (
    <PointsStore>
      <GroupsList />
    </PointsStore>
  );
}

export default Root;
