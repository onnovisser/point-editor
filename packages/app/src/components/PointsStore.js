import produce from 'immer';
import React, { createContext, useReducer, useEffect } from 'react';
import uuid from 'uuid/v4';

const pointsStoreContext = createContext();

const actions = {
  addGroup(state, { radius }) {
    state.groups.push({ id: uuid(), radius, points: [] });
  },
  removeGroup(state, { id }) {
    state.groups = state.groups.filter(group => group.id !== id);
  },
  editGroup(state, { id, radius }) {
    state.groups.forEach(group => {
      if (group.id === id) {
        group.radius = radius;
      }
    });
  },
  addPointToGroup(state, { groupId, point }) {
    const group = state.groups.find(g => g.id === groupId);
    if (group) {
      group.points.push({ id: uuid(), point });
    }
  },
  removePointFromGroup(state, { groupId, pointId }) {
    const group = state.groups.find(g => g.id === groupId);
    if (group) {
      group.points = group.points.filter(point => point.id !== pointId);
    }
  },
  editPoint(state, { groupId, pointId, point }) {
    const group = state.groups.find(g => g.id === groupId);
    if (group) {
      group.points.forEach((p, i) => {
        if (p.id === pointId) {
          group.points[i].point = point;
        }
      });
    }
  },
};

const reducer = produce((state, { type, payload }) => {
  actions[type](state, payload);
});

const persisted = localStorage.getItem('points-persist')
const initialState = persisted ? JSON.parse(persisted) : {
  groups: [],
};

function PointsStore({ children }) {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    localStorage.setItem('points-persist', JSON.stringify(state))
  })

  return (
    <pointsStoreContext.Provider value={[state, dispatch]}>
      {children}
    </pointsStoreContext.Provider>
  );
}

export default PointsStore;
export { pointsStoreContext };
