import { ErrorMessage, Field, Form, Formik } from 'formik';
import { func, number, shape, string, arrayOf } from 'prop-types';
import React from 'react';
import { arePointsInCircle } from 'point-utils';

GroupForm.propTypes = {
  group: shape({
    radius: number.isRequired,
    points: arrayOf(shape({
      point: shape({
        x: number.isRequired,
        y: number.isRequired
      }).isRequired
    })).isRequired
  }),
  onSubmit: func.isRequired,
  initialValues: shape({
    radius: number.isRequired,
  }),
  submitLabel: string.isRequired,
};

function GroupForm({ group, onSubmit, initialValues = { radius: 0 }, submitLabel }) {
  return (
    <Formik
      initialValues={initialValues}
      validate={({ radius }) => {
        const errors = {};
        if (!Number.isFinite(radius) || radius <= 0) {
          errors.radius = 'Not a valid radius';
        }
        if (group && !arePointsInCircle(group.points.map(p => p.point), radius)) {
          errors.radius = 'Group has points that aren\'t in the new radius';
        }

        return errors;
      }}
      onSubmit={({ radius }, actions) => {
        actions.resetForm();
        onSubmit({ radius });
      }}
    >
      {() => (
        <Form>
          <Field name="radius" type="number" step="any" min="0" />
          <ErrorMessage name="radius" />
          <button type="submit">{submitLabel}</button>
        </Form>
      )}
    </Formik>
  );
}

export default GroupForm;
