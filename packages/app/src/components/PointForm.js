import { ErrorMessage, Field, Form, Formik } from 'formik';
import { createPoint2D, isPointInCircle } from 'point-utils';
import { func, number, shape, string } from 'prop-types';
import React from 'react';

PointForm.propTypes = {
  group: shape({
    radius: number.isRequired,
  }).isRequired,
  onSubmit: func.isRequired,
  initialValues: shape({
    x: number.isRequired,
    y: number.isRequired,
  }),
  submitLabel: string.isRequired,
};

function PointForm({ group, onSubmit, initialValues = { x: 0, y: 0 }, submitLabel }) {
  return (
    <Formik
      initialValues={initialValues}
      validate={({ x, y }) => {
        const errors = {};
        if (!Number.isFinite(x) || !Number.isFinite(y)) {
          errors.x = 'Not valid coords';
        } else if (!isPointInCircle(createPoint2D(x, y), group.radius)) {
          errors.x = "Point doesn't lie in the circle";
        }

        return errors;
      }}
      onSubmit={({ x, y }, actions) => {
        actions.resetForm();
        onSubmit(createPoint2D(x, y));
      }}
    >
      {() => (
        <Form>
          <Field name="x" type="number" step="any" />
          <Field name="y" type="number" step="any" />
          <ErrorMessage name="x" />
          <button type="submit">{submitLabel}</button>
        </Form>
      )}
    </Formik>
  );
}

export default PointForm;
