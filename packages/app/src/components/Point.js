import React, { useState } from 'react';
import PointForm from './PointForm';
import { shape, number, func } from 'prop-types';

Point.propTypes = {
  group: shape({
    radius: number.isRequired
  }).isRequired,
  onRemove: func.isRequired,
  onChange: func.isRequired,
  point: shape({
    x: number.isRequired,
    y: number.isRequired,
  }).isRequired,
};

function Point({ group, onRemove, onChange, point }) {
  const [isEditing, setIsEditing] = useState(false);

  function handleEditSubmit(newPoint) {
    setIsEditing(false);
    onChange(newPoint);
  }

  return (
    <div>
      {isEditing ? (
        <>
          <PointForm
            group={group}
            onSubmit={handleEditSubmit}
            initialValues={point}
            submitLabel="confirm"
          />
          <button onClick={() => setIsEditing(false)}>cancel</button>
        </>
      ) : (
        <>
          <span>
            ({point.x}, {point.y})
          </span>
          <button onClick={() => setIsEditing(true)}>edit</button>
          <button onClick={onRemove}>remove</button>
        </>
      )}
    </div>
  );
}

export default Point;
