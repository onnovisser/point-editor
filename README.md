# Task

Imagine you are developing an app step by step. Tasks appear as you develop the app. Make a new git-repository and solve each task. Remember to open access to your work and send us a link.

1. Create a point with the coordinates (x, y). Write a reusable function (or class method) showing a point inside a circle, with radius R, centered on coordinates (0, 0).
2. Now, write a function to process an array of points.
3. How would you rewrite this code for a three-dimensional case (with 3 coordinates at the point: x,y,z and a sphere instead of the circle)?
4. Make an npm package with the necessary code. You don’t have to publish it .
5. Make a new repository, init React.js application and connect a package.
6. Allow users to make a list of points that lay inside the sphere. They should be able to create a new group that identifies the radius of circle. Then they will add points to this group and the app will tell them which ones are suitable. Users can save or delete the point, If they save, the point must be in the group after a page reload. The group should be able to be saved, deleted, or edited.

# Notes

This project uses yarn workspaces. To install the dependencies run `yarn`. To run the project, run `yarn start` in `packages/app`.
To install if you don't have yarn installed, run `npm link` in `packages/point-utils`, Then in `packages/app` run `npm link point-utils && npm i`. Then run `npm start` to start.

`packages/point-utils` contains the NPM package from steps 1 - 4. `packages/app` contains the React app.

## Functionality

* A user can add/edit/remove groups
* A user can add 2d points to a group if they lie inside the radius of the group. They can also edit/remove them.
* A user can change the radius of a group only if the new radius is equal or larger than the furthest point in the group
* Changes are persisted to LocalStorage

## Next steps

* Add tests to the React app
* Improve the UI (form labels, styling, ...)
* Refactor some parts, add comments
* Add support for 3D points
* Visualise the circle/spheres and points somehow
